#!/usr/bin/env bash

mapfile -t foreign < <(pacman -Qqm | sort)

# echo "${foreign[@]}"

yay_cachedir="${XDG_CACHE_HOME:-$HOME/.cache}/yay"

# folders="$(for i in "$(ls -d $yay_cachedir/*/ )"; do echo "${i%%/}"; done)"
folders=("$yay_cachedir"/*/)
folders=("${folders[@]%/}")
folders=("${folders[@]##*/}")

# echo "${folders[@]}"

mapfile -t installed < <(comm -12 --check-order \
    <(printf '%s\n' "${foreign[@]}") \
    <(printf '%s\n' "${folders[@]}"))

mapfile -t missing < <(comm -13 --check-order \
    <(printf '%s\n' "${foreign[@]}") \
    <(printf '%s\n' "${folders[@]}"))

for pkg in "${installed[@]}"; do
    paccache -r -c "$yay_cachedir/$pkg" -v
done

for pkg in "${missing[@]}"; do
    find "$yay_cachedir/$pkg" -type f -mtime +30 -exec rm --interactive=never '{}' ';'
    count=$(find "$yay_cachedir/$pkg" -type f -print | wc -l)
    if [[ $count == 0 ]]; then
        find "$yay_cachedir/$pkg" -type d -empty -delete && echo "Removed $pkg folder."
    fi
done
