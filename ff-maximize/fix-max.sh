#!/bin/sh

PROFILE=~/.mozilla/firefox/*.default

if [ -e $PROFILE/sessionstore.js ] ; then
  cp $PROFILE/sessionstore.js $PROFILE/sessionstore.js.fixmaxbak
  jq -c '.windows[0].sizemode="maximized"' $PROFILE/sessionstore.js.fixmaxbak > $PROFILE/sessionstore.js.fixmax
  mv $PROFILE/sessionstore.js.fixmax $PROFILE/sessionstore.js
fi
